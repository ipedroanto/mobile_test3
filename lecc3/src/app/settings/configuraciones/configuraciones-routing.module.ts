import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";
import { ConfFormComponent } from "./configuraciones-form..component";

import { ConfComponent } from "./configuraciones.componet";

const routes: Routes = [
    { path: "", component: ConfComponent },
    { path: "ConfForm", component: ConfFormComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ConfiguracionesRoutingModule { }