import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store"
import * as Toast from "nativescript-toast"
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application} from "@nativescript/core";
import { NoticiasService} from "../domain/noticias.service";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction} from "../domain/noticias-state.model";
import { toBase64String } from "@angular/compiler/src/output/source_map";


@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html",
    // providers: [NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    // constructor(public noticias: NoticiasService) {
    constructor(
        private noticias: NoticiasService,
        private store: Store<AppState>
    ){
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
           .subscribe((data) => {
               const f = data;
               if (f != null) {
                //    Toast.show((text: "Sugerimos leer: " +f.titulo, curation: Toast.duration.SHORT));
                   Toast.makeText("Sugerimos leer: " +f.titulo).show();
               }
           });

    }
    
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void{
        // console.dir(x);
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }  
    buscarAhora(s: string) {
        console.dir("buscarAhora" +s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " +e);
            // Toast.show({text: "Error en la busqueda", duration: Toast.duration.SHORT});
            Toast.makeText("Error en la busqueda").show();
        });

        }
    }       

