import { Component, EventEmitter, Input, Output} from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { ngModel } from '@angular/core';
// import { textAlignmentProperty } from '@nativescript/core';

@Component({
  selector: 'SearchForm',
  moduleId: module.id,
  // templateUrl: "./search-form.component.html",
  template:`
        <TextField [(ngModel)]="textFieldValue" hint="Ingresar texto..."></TextField>
        <Button text="Buscar" (tap)="onButtonTap()"></Button>`
       
  
})
export class SearchFormComponent {
  textFieldValue: string = "";
  @Output() search: EventEmitter<string> = new EventEmitter();
  @Input() inicial: string;

  ngOnInit(): void {
    this.textFieldValue = this.inicial;
  }
  onButtonTap(): void {
    console.log(this.textFieldValue);
    if(this.textFieldValue.length >2){
      this.search.emit(this.textFieldValue);
    }
  }

}