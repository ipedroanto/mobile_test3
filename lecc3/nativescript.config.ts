import { NativeScriptConfig } from '@nativescript/core';

export default {
  id: 'org.nativescript.lecc2',
  appResourcesPath: 'App_Resources',
  appPath: "src",
  android: {
    v8Flags: '--expose_gc',
    markingMode: 'none'
  }
} as NativeScriptConfig;