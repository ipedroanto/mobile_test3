import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule,  NativeScriptFormsModule } from "@nativescript/angular";

import { ConfiguracionesRoutingModule } from "./configuraciones-routing.module";
import { ConfComponent } from "./configuraciones.componet";
import { ConfFormComponent } from "./configuraciones-form..component";
import { MinLenDirective } from "./minlen.validator.username";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ConfiguracionesRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        ConfComponent,
        ConfFormComponent,
        MinLenDirective

    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ConfiguracionesModule { }
