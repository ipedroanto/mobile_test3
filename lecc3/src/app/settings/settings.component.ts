import { Component, OnInit } from "@angular/core";
import * as Toast from "nativescript-toast";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Dialogs } from "@nativescript/core";
// import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn,1000); }

    ngOnInit(): void {
        // this.doLater(() =>
        // Dialogs.action("Mensaje","Cancelar", ["Option1","Option2"])
        //         .then((result) => {
        //                            console.log("resultado: " + result);
        //                            if (result === "Option1") {
        //                                this.doLater(() =>
        //                                Dialogs.alert({
        //                                    title: "Titulo 1 ",
        //                                    message: "mje 1 ",
        //                                    okButtonText: "btn 1"
        //                                }).then(() => console.log("Cerrado 1!")));
        //                            } else if (result === "Option2") {
        //                                this.doLater(() =>
        //                                   Dialogs.alert({
        //                                       title: "Titulo 2",
        //                                       message: "mje 2",
        //                                       okButtonText: "btn 2"
        //                                   }).then(() => console.log("Cerrado 2!")));
        //                            }
        //         }));
        // const toastOptions: Toast.ToastOptions = (text: "Hello Word", duration: Toast.duration.SHORT);
        // this.doLater(() => Toast.show(toastOptions));
        Toast.makeText("Hello World").show();
    }        


    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
